import random

parte1 = [
    "O sistema de desenvolvimento",
    "O novo protocolo de otimização",
    "O algoritmo de otimização"
    ]

parte2 = [
    "possui excelente desempenho",
    "oferece garantias de precisão acima da média",
    "preenche uma lacuna significativa"
]

parte3 = [
    "nas aplicações a que se destina.",
    "em relação às opções disponíveis no mercado.",
    ", promovendo ampla vantagem competitiva a seus usuários."
]

diminutivo = int(input("Escolha a lingua: 0 - normal; 1 - diminutivo"))

if diminutivo == 2:
    parte1 = []
    parte2 = []
    parte3 = []
    
print (random.choice(parte1), random.choice(parte2), random.choice(parte3))
